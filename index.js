'use strict';

const IpURL = 'https://api.ipify.org/?format=json';
const UserURL = 'http://ip-api.com/json';

const btn = document.querySelector('button');

btn.addEventListener('click', async () => {
    try {
        const IpResponse = await axios.get(IpURL);
        const user = await axios.get(`${UserURL}/${IpResponse.data.ip}?fields=1699839`);
        const card = document.createElement('div')
        card.innerHTML = `
        <span>Континент: ${user.data.continent}</span>
        <span>Країна: ${user.data.country}</span>
        <span>Регіон: ${user.data.region}</span>
        <span>Місто: ${user.data.city}</span>
        `;
        const body = document.querySelector('body');
        body.append(card)
    }catch(err) {
        console.log(err)
    }
})

